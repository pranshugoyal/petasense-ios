//
//  GraphPoint.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 08/06/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import Foundation

private let formatter: DateFormatter = {
    let f = DateFormatter()
    f.dateFormat = "E, dd MMM yyyy HH:mm:ss zzz"
    return f
}()

struct GraphPoint: Comparable
{
    let id: Int
    let value: Float
    let date: Date
    
    init(id: Int, value: Float, dateString: String)
    {
        self.id = id
        date = formatter.date(from: dateString)!
        self.value = value
    }
    
    static func ==(lhs: GraphPoint, rhs: GraphPoint) -> Bool
    {
        return lhs.value == rhs.value
    }
    
    static func <(lhs: GraphPoint, rhs: GraphPoint) -> Bool
    {
        return lhs.value < rhs.value
    }
    
    static func smallerX(lhs: GraphPoint, rhs: GraphPoint) -> Bool
    {
        return lhs.date < rhs.date
    }
}

extension GraphPoint
{
    static func parsePoints(from data: NSDictionary) -> [GraphPoint]
    {
        let trend = data["trend_data"] as! [String: [Any]]
        let trendData = trend["data"] as! [Float]
        let ids = trend["measurement_id"] as! [Int]
        let times = trend["time"] as! [String]
        
        let count = min(trendData.count, ids.count, times.count)
        return (0..<count).map({GraphPoint(id: ids[$0], value: trendData[$0], dateString: times[$0])})
    }
}
