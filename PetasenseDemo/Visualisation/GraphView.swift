//
//  GraphView.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 08/06/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

@IBDesignable
class GraphView: UIView
{
    private let graphInsets = UIEdgeInsets(top: 20, left: 30, bottom: 25, right: 20)
    private let dateFormatter = DateFormatter()
    
    var graphs: [(points: [GraphPoint], color: UIColor)] = []
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setInitialProperties()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setInitialProperties()
    }
    
    private func setInitialProperties()
    {
        backgroundColor = .white
        isOpaque = true
        clearsContextBeforeDrawing = true
    }
    
    var graphRect: CGRect
    {
        return UIEdgeInsetsInsetRect(bounds, graphInsets)
    }
    
    override func draw(_ rect: CGRect)
    {
        guard !graphs.isEmpty else
        {
            print("Data not provided")
            return
        }
        
        let translationTransform = CGAffineTransform(translationX: graphInsets.left, y: graphInsets.top)
        let graphZ = Curve(with: graphs[0].points, and: graphRect.size)
        
        //Create graph curve
        for graph in graphs
        {
            graph.color.setStroke()
            let curve = Curve(with: graph.points, and: graphRect.size).createPath()
            curve.apply(translationTransform)
            curve.lineWidth = 2
            curve.stroke()
        }
        
        createGridAndLabels(from: graphZ)
    }
    
    private func createGridAndLabels(from curve: Curve)
    {
        let grid = UIBezierPath()
        
        //Draw horizontal graph lines on the top of everything
        let lineSpacing = graphRect.height/5
        
        for i in 0..<6
        {
            let y = bounds.height - graphInsets.bottom - CGFloat(i)*lineSpacing
            grid.move(to: CGPoint(x:graphInsets.left - 5, y: y))
            grid.addLine(to: CGPoint(x: bounds.width - graphInsets.right + 5, y:y))
            
            let label = newGridLabel()
            label.center.y = bounds.height - graphInsets.bottom - CGFloat(i)*lineSpacing
            label.text = "\(i).0"
            addSubview(label)
        }
        
        //Draw vertical graph lines
        for i in 0..<curve.points.count
        {
            let gridGap = 10
            guard i%gridGap == 0 else {
                continue
            }
            let x = curve.columnXPoint(column: i) + graphInsets.left
            grid.move(to: CGPoint(x: x, y: graphInsets.top - 5))
            grid.addLine(to: CGPoint(x: x, y: bounds.height - graphInsets.bottom + 5))
            
            let label = newGridLabel()
            label.frame.size.width = curve.spacing*CGFloat(gridGap)/2
            label.frame.origin.y = bounds.maxY - graphInsets.bottom + 10
            label.center.x = x
            label.textAlignment = .center
            label.text = getDateString(from: curve.points[i], format: "MMM dd")
            addSubview(label)
        }
        
        #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1274207746).setStroke()
        grid.lineWidth = 1.0
        grid.stroke()
    }
    
    private func newGridLabel() -> UILabel
    {
        let label = UILabel(frame: CGRect(x: 5, y: 0, width: 20, height: 15))
        label.backgroundColor = .clear
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        label.font = UIFont.systemFont(ofSize: 11)
        return label
    }
    
    private func getDateString(from point: GraphPoint, format: String) -> String
    {
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: point.date)
    }
}
