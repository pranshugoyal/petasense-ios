//
//  DataDisplayController.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 6/8/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

class DataDisplayController: UIViewController
{
    @IBOutlet weak var graphScroll: UIScrollView!
    
    private var displayedGraph: GraphView?
    private let vibrationData = VibrationData()
    private var currentlyDisplayed: [VibrationData.Axis: Bool] = [.x: false, .y: false, .z: true]
    
    private var canInteract: Bool = true
    
	override func viewDidLoad()
	{
		super.viewDidLoad()
        fetchAndDisplayData(for: .z)
	}
    
    func fetchAndDisplayData(for axis: VibrationData.Axis, completion: ((Bool) -> Void)? = nil)
    {
        canInteract = false
        vibrationData.fetchData(for: axis)
        { (points, error) in
            if let error = error
            {
                self.presentAlert(withMessage: error.localizedDescription)
                completion?(false)
                return
            }
            
            self.refreshGraph()
            completion?(true)
            self.canInteract = true
        }
    }
    
    func refreshGraph()
    {
        displayedGraph?.removeFromSuperview()
        displayedGraph = nil
        var displayedGraphs: [(points: [GraphPoint], color: UIColor)] = []
        var maxCount = 0
        for (axis, shouldDisplay) in currentlyDisplayed where shouldDisplay
        {
            displayedGraphs.append((vibrationData[axis], axis.color))
            if vibrationData[axis].count > maxCount
            {
                maxCount = vibrationData[axis].count
            }
        }

        let graphSize = CGSize(width: CGFloat(maxCount*10 + 50), height: graphScroll.frame.height)
        let graphView = GraphView(frame: CGRect(origin: .zero, size: graphSize))
        graphView.graphs = displayedGraphs
        graphScroll.contentSize = graphSize
        graphScroll.addSubview(graphView)
        graphView.setNeedsDisplay()
        displayedGraph = graphView
    }
    
    @IBAction func vibZPressed(_ sender: UIButton)
    {
        guard canInteract else
        {
            print("Already fetching data: Interactions ignored")
            return
        }
        let axis = axisFromButtonTag(sender.tag)
        currentlyDisplayed[axis]?.toggle()
        
        guard !currentlyDisplayed.values.filter({$0}).isEmpty else
        {
            print("Not all graphs can be set hidden, can not hide axis: \(axis.rawValue)")
            currentlyDisplayed[axis]?.toggle()
            return
        }
        
        if currentlyDisplayed[axis]!
        {
            if !vibrationData[axis].isEmpty
            {
                refreshGraph()
            }
            else
            {
                fetchAndDisplayData(for: axis)
            }
        }
        else
        {
            refreshGraph()
        }
    }
    
    private func axisFromButtonTag(_ tag: Int) -> VibrationData.Axis
    {
        switch tag
        {
            case 1:
                return .x
            case 2:
                return .y
            case 3:
                return .z
            default:
                fatalError("Invalid button tag received, should be in 1..<3")
        }
    }
}

private extension Bool
{
    mutating func toggle()
    {
        self = !self
    }
}
