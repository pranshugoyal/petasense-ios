//
//  UIViewController+Util.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 08/06/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

extension UIViewController
{
    func presentAlert(withMessage message: String, title: String = "Error", completion: (() -> Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: completion)
    }
}
