//
//  ViewController.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 06/06/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController
{
	@IBOutlet weak var usernameTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!

	@IBAction func loginPressed(_ sender: UIButton)
	{
		guard let username = usernameTextField.text,
			  let password = passwordTextField.text,
              !username.isEmpty, !password.isEmpty
		else
		{
			presentAlert(withMessage: "Credentials not entered")
			return
		}

		let parameters = ["username": username, "password":password]
		NetworkHandler(APIEndPoints.login)?.makePostRequest(paramereters: parameters)
		{ (response, error) in
			if let error = error
			{
				self.presentAlert(withMessage: error.localizedDescription)
			}
			else
			{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataDisplayController")
                self.navigationController?.pushViewController(vc!, animated: true)
			}
		}
	}

	override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?)
	{
		if event?.type == .motion && motion == .motionShake
		{
			#if DEBUG
				usernameTextField.text = "test@petasense.com"
				passwordTextField.text = "pYEMZU3ggJj9u7XD"
			#endif
		}
	}
}

