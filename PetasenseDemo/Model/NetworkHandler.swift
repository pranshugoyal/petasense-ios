//
//  NetworkHandler.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 6/8/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

class NetworkHandler
{
	let url: URL
    private var completionClosure: ((NSDictionary?, Error?) -> Void)?

	init?(_ endpoint: String)
	{
		if let u = URL(string: endpoint)
		{
			url = u
		}
		else
		{
			return nil
		}
	}

	func makePostRequest(paramereters: [String: Any], completion: @escaping (NSDictionary?, Error?) -> Void)
	{
        completionClosure = completion
		makeRequest(paramereters: paramereters, requestType: "POST")
	}
    
    func makeGetRequest(paramereters: [String: Any], completion: @escaping (NSDictionary?, Error?) -> Void)
    {
        completionClosure = completion
        makeRequest(paramereters: paramereters, requestType: "GET")
    }
    
    private func makeRequest(paramereters: [String: Any], requestType type: String)
    {
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = type
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
        
        let bodyData = paramereters.getParametersAsString().data(using: .utf8, allowLossyConversion: true)
        request.httpBody = bodyData
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let task = URLSession.shared.dataTask(with: request)
        {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            if let error = error
            {
                self.callback(data: nil, error: error)
            }
            else
            {
                do
                {
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    self.callback(data: jsonResult, error: nil)
                }
                catch
                {
                    self.callback(data: nil, error: error)
                }
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        task.resume()
    }
    
    private func callback(data: NSDictionary?, error: Error?)
    {
        DispatchQueue.main.async {
            self.completionClosure?(data, error)
        }
    }
}

private extension Dictionary where Key: CustomStringConvertible
{
	func getParametersAsString() -> String
	{
		return map(getKeyValueAsString).joined(separator: "&")
	}

	private func getKeyValueAsString(key: Any, value: Any) -> String
	{
		return "\((key))=\((value as AnyObject))"
	}
}
