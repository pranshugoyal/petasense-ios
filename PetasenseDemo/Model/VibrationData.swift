//
//  VibrationData.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 10/06/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

class VibrationData
{
    private var vibX: [GraphPoint] = []
    private var vibY: [GraphPoint] = []
    private var vibZ: [GraphPoint] = []
    
    enum Axis: String
    {
        case x, y, z
        
        var color: UIColor
        {
            switch self
            {
            case .x:
                return #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            case .y:
                return #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            case .z:
                return #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
        }
    }
    
    var maxCount: Int
    {
        return max(vibX.count, vibY.count, vibZ.count)
    }
    
    func fetchData(for axis: Axis, completion: @escaping ([GraphPoint], Error?) -> Void)
    {
        NetworkHandler(APIEndPoints.getVibrationData)?.makeGetRequest(paramereters: [:])
        { (response, error) in
            if let error = error
            {
                completion([], error)
            }
            else if let data = response
            {
                self[axis] = GraphPoint.parsePoints(from: data)
                completion(self[axis], nil)
            }
        }
    }
    
    private(set) subscript(_ axis: Axis) -> [GraphPoint]
    {
        get
        {
            switch axis
            {
                case .x:
                    return vibX
                case .y:
                    return vibY
                case .z:
                    return vibZ
            }
        }
        set
        {
            switch axis
            {
                case .x:
                    vibX = newValue
                case .y:
                    vibY = newValue
                case .z:
                    vibZ = newValue
            }
        }
    }
}
