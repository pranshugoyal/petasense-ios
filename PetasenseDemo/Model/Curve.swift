//
//  Graph.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 10/06/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import UIKit

class Curve
{
    let points: [GraphPoint]
    private let frame: CGRect
    
    var spacing: CGFloat
    private let maxValue: GraphPoint
    private let minValue: GraphPoint
    
    init(with points: [GraphPoint], and size: CGSize)
    {
        self.points = points
        frame = CGRect(origin: .zero, size: size)
        maxValue = points.max()!
        minValue = points.min()!
        spacing = frame.width/CGFloat(points.count)
    }
    
    func createPath() -> UIBezierPath
    {
        let graphPath = UIBezierPath()
        
        var p1 = locationForGraphPoint(at: 0)
        graphPath.move(to: p1)
        
        for index in 1..<points.count
        {
            let p2 = locationForGraphPoint(at: index)
            let mid = midPoint(for: p1, and: p2)
            let control1 = controlPoint(for: mid, and: p1)
            let control2 = controlPoint(for: mid, and: p2)
            
            graphPath.addQuadCurve(to: mid, controlPoint: control1)
            graphPath.addQuadCurve(to: p2, controlPoint: control2)
            p1 = p2
        }
        return graphPath
    }
    
    func columnXPoint(column: Int) -> CGFloat
    {
        return CGFloat(column) * spacing
    }
    
    private func columnY(point graphPoint: GraphPoint) -> CGFloat
    {
        var y:CGFloat = CGFloat(graphPoint.value) / CGFloat(maxValue.value - minValue.value) * frame.height
        y = frame.height - y
        return y
    }
    
    private func locationForGraphPoint(at index: Int) -> CGPoint
    {
        return CGPoint(x: columnXPoint(column: index), y: columnY(point: points[index]))
    }
}

private func midPoint(for p1: CGPoint, and p2: CGPoint) -> CGPoint
{
    return CGPoint(x: (p1.x+p2.x)/2, y: (p1.y+p2.y)/2)
}

private func controlPoint(for p1: CGPoint, and p2: CGPoint) -> CGPoint
{
    var controlPoint = midPoint(for: p1, and: p2)
    let diffY = abs(p2.y - controlPoint.y)
    
    if p1.y < p2.y
    {
        controlPoint.y += diffY;
    }
    else if p1.y > p2.y
    {
        controlPoint.y -= diffY;
    }
    
    return controlPoint;
}
