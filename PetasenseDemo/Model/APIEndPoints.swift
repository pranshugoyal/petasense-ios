//
//  APIEndPoints.swift
//  PetasenseDemo
//
//  Created by Pranshu Goyal on 6/8/17.
//  Copyright © 2017 Pranshu Goyal. All rights reserved.
//

import Foundation

struct APIEndPoints
{
	static let baseUrl = Bundle.main.infoDictionary!["BASE_URL"] as! String

	static let login = baseUrl + "login"

	static let getVibrationData = baseUrl + "webapp/vibration-data/129/broadband-trend"
}
